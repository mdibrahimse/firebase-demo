package com.appriverr.firbasepractise;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    private TextView etsignup;
    private EditText etemail,etpassword;
    private Button submit;
    private ProgressBar progressBar;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        etemail = findViewById(R.id.etEmail);
        etpassword = findViewById(R.id.etPass);
        submit = findViewById(R.id.btSend);
        etsignup = findViewById(R.id.txSignup);
        progressBar = findViewById(R.id.progressbar);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogin();

                //Toast.makeText(MainActivity.this, "Data : "+userEmail+" "+userPass, Toast.LENGTH_SHORT).show();
            }
        });




        //SignUp linking
        etsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),SignupActivity.class);
                startActivity(intent);
                //Toast.makeText(MainActivity.this, "Testing,,,,,,,,,", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void userLogin() {
        String email = etemail.getText().toString();
        String password = etpassword.getText().toString();
        if(email.isEmpty())
        {
            etemail.setError("Enter an email address");
            etemail.requestFocus();
            return;
        }

        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            etemail.setError("Enter a valid email address");
            etemail.requestFocus();
            return;
        }

        //checking the validity of the password
        if(password.isEmpty())
        {
            etpassword.setError("Enter a password");
            etpassword.requestFocus();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.GONE);
                if (task.isSuccessful()){
                    finish();
                    Intent intent = new Intent(getApplicationContext(),DashboardActivity.class);
                    intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }else{
                    Toast.makeText(MainActivity.this, "Login Unsuccessfull !!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
